import {
    app,
    BrowserWindow
} from 'electron';
import env from './../env';


export default function(mainWindow) {
    var mainMenuTemplate;
    if (env.name !== 'production') {
        mainMenuTemplate = [{
            label: 'Editar',
            submenu: [{
                role: 'undo'
            }, {
                role: 'redo'
            }, {
                type: 'separator'
            }, {
                role: 'cut'
            }, {
                role: 'copy'
            }, {
                role: 'paste'
            }, {
                role: 'delete'
            }, {
                role: 'selectall'
            }]
        }, {
            label: 'Visualizar',
            submenu: [{
                label: 'Recarregar',
                accelerator: 'CmdOrCtrl+R',
                click(item, focusedWindow) {
                    if (focusedWindow)
                        focusedWindow.reload();
                }
            }, {
                label: 'Colocar em tela cheia',
                role: 'togglefullscreen'
            }, {
                label: 'Ferramenta do Desenvolvedor',
                accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I',
                click(item, focusedWindow) {
                    if (focusedWindow)
                        focusedWindow.webContents.toggle
                }
            }]
        }, {
            label: 'Janela',
            role: 'window',
            submenu: [{
                role: 'minimize'
            }, {
                role: 'close'
            }]
        }, {
            label: 'Ajuda',
            role: 'help',
            submenu: [{
                label: 'Procurar atualizações',
                click(item, focusedWindow) {
                    mainWindow.webContents.send('doControllCall', 'checkUpdate');
                }
            }]
        }];

        if (process.platform === 'darwin') {
            mainMenuTemplate.unshift({
                label: app.getName(),
                submenu: [{
                    label: `Sobre ${app.getName()}`,
                    role: 'about'
                }, {
                    type: 'separator'
                }, {
                    role: 'services',
                    submenu: []
                }, {
                    type: 'separator'
                }, {
                    role: 'hide'
                }, {
                    role: 'hideothers'
                }, {
                    role: 'unhide'
                }, {
                    type: 'separator'
                }, {
                    role: 'quit'
                }]
            });
            mainMenuTemplate[3].submenu = [{
                label: 'Fechar',
                accelerator: 'CmdOrCtrl+W',
                role: 'close'
            }, {
                label: 'Minimizar',
                accelerator: 'CmdOrCtrl+M',
                role: 'minimize'
            }, {
                label: 'Ampliar',
                role: 'zoom'
            }, {
                type: 'separator'
            }, {
                label: 'Trazer todas as janelas para a frente',
                role: 'front'
            }]
        }
    } else {
        mainMenuTemplate = [{
            label: 'Editar',
            submenu: [{
                label: 'Desfazer',
                role: 'undo'
            }, {
                label: 'Refazer',
                role: 'redo'
            }, {
                type: 'separator'
            }, {
                label: 'Recortar',
                role: 'cut'
            }, {
                label: 'Copiar',
                role: 'copy'
            }, {
                label: 'Colar',
                role: 'paste'
            }, {
                label: 'Remover',
                role: 'delete'
            }, {
                label: 'Selecionar Tudo',
                role: 'selectall'
            }]
        }, {
            label: 'Visualizar',
            submenu: [{
                role: 'togglefullscreen'
            }]
        }, {
            label: 'Janela',
            role: 'window',
            submenu: [{
                label: 'Minimizar',
                role: 'minimize'
            }, {
                label: 'Fechar',
                role: 'close'
            }]
        }, {
            label: 'Ajuda',
            role: 'help',
            submenu: [{
                label: 'Procurar atualizações',
                click(item, focusedWindow) {
                    mainWindow.webContents.send('doControllCall', 'checkUpdate');
                }
            }, {
                label: `Versão ${app.getVersion()}`,
                enabled: false
            }]
        }];

        if (process.platform === 'darwin') {
            mainMenuTemplate.unshift({
                label: app.getName(),
                submenu: [{
                    role: 'about'
                }, {
                    type: 'separator'
                }, {
                    role: 'services',
                    submenu: []
                }, {
                    type: 'separator'
                }, {
                    role: 'hide'
                }, {
                    role: 'hideothers'
                }, {
                    role: 'unhide'
                }, {
                    type: 'separator'
                }, {
                    role: 'quit'
                }]
            });
            mainMenuTemplate[3].submenu = [{
                label: 'Fechar',
                accelerator: 'CmdOrCtrl+W',
                role: 'close'
            }, {
                label: 'Minimizar',
                accelerator: 'CmdOrCtrl+M',
                role: 'minimize'
            }, {
                label: 'Ampliar',
                role: 'zoom'
            }, {
                type: 'separator'
            }, {
                label: 'Trazer todas as janelas para a frente',
                role: 'front'
            }]
        }
    }
    return mainMenuTemplate;
};