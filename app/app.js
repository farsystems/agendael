(function() {
    'use strict';

    var _templateBase = './scripts';

    angular.module('app', [
            'ngRoute',
            'ngMaterial',
            'ngAnimate',
            'ui.utils.masks',
            'angular-inview',
            'duScroll'
        ])
        .config(function($mdThemingProvider, $mdIconProvider) {
            $mdIconProvider
                .defaultIconSet("./assets/svg/avatars.svg", 128)
                .icon("menu", "./assets/svg/menu.svg", 24)
                .icon("share", "./assets/svg/share.svg", 24)
                .icon("google_plus", "./assets/svg/google_plus.svg", 512)
                .icon("hangouts", "./assets/svg/hangouts.svg", 512)
                .icon("twitter", "./assets/svg/twitter.svg", 512)
                .icon("cog", "./assets/svg/cog.svg", 512)
                .icon("config", "./assets/svg/config.svg", 512)
                .icon("search", "./assets/svg/search.svg", 512)
                .icon("phone", "./assets/svg/phone.svg", 512);

            $mdThemingProvider.theme('default')
                .primaryPalette('indigo')
                .accentPalette('orange');
            $mdThemingProvider.theme('altTheme')
                .primaryPalette('blue');
        })
        .config(['$routeProvider', function($routeProvider) {
            $routeProvider.when('/', {
                templateUrl: _templateBase + '/contato/contato.html',
                controller: 'contatoController',
                controllerAs: '_ctrl'
            });
            $routeProvider.otherwise({
                redirectTo: '/'
            });
        }]);

})();