import {
    ipcRenderer
} from 'electron';

(function () {
    'use strict';

    angular.module('app')
        .directive('focusOn', function ($timeout) {
            return function (scope, elem, attr) {
                scope.$on(attr.focusOn, function (e) {
                    $timeout(function () {
                        elem[0].focus();
                    }, 100);
                });
            };
        })
        .controller('contatoController', ['contatoService', '$scope', '$q', '$mdDialog', '$mdToast', '$mdSidenav', '$mdUtil', '$timeout', '$location', '$anchorScroll', ContatoController]);

    function ContatoController(contatoService, $scope, $q, $mdDialog, $mdToast, $mdSidenav, $mdUtil, $timeout, $location, $anchorScroll) {
        var self = this;

        self.selected = {};
        self.contatos = [];
        self.filterText = null;
        self.oldContato = {};
        self.editando = false;
        self.alterado = false;
        self.doBanco = false;
        self.toggleMenu = toggleMenu;
        self.isOpenMenu = isOpenMenu;
        self.selectContato = selectContato;
        self.deleteContato = deleteContato;
        self.saveContato = saveContato;
        self.createContato = createContato;
        self.filter = filterContato;
        self.editarContato = editarContato;
        self.cancelarEdicao = cancelarEdicao;
        self.adicionarTelefone = adicionarTelefone;
        self.adicionarEndereco = adicionarEndereco;
        self.checkFilters = checkFilters;
        self.contactInView = contactInView;
        self.processKeyContatoSearch = processKeyContatoSearch;
        self.tiposTelefone = [
            "Telefone",
            "Celular",
            "Fax",
            "Outro"
        ];
        self.tiposEndereco = [
            "Casa",
            "Trabalho",
            "Outro"
        ];
        self.filters = [{
            name: "Nome",
            field: "nome",
            description: "Filtrar pelo nome",
            value: true
        }, {
            name: "Sobrenome",
            field: "sobrenome",
            description: "Filtrar pelo sobrenome",
            value: true
        }, {
            name: "Descrição",
            field: "descricao",
            description: "Filtrar pela descrição",
            value: true
        }, {
            name: "E-mail",
            field: "email",
            description: "Filtrar pelo e-mail",
            value: true
        }];


        // Load initial data
        getAllContatos();

        function debounce(func, wait, context) {
            var timer;
            return function debounced() {
                var context = $scope,
                    args = Array.prototype.slice.call(arguments);
                $timeout.cancel(timer);
                timer = $timeout(function () {
                    timer = undefined;
                    func.apply(context, args);
                }, wait || 10);
            };
        }

        //----------------------
        // Internal functions
        //----------------------

        function checkFilters(item) {
            if (self.filters[item].value == true) return;

            var haveFilter = false;
            for (let i = 0; i < self.filters.length; i++) {
                if (i != item && self.filters[i].value == true) {
                    haveFilter = true;
                }
            }

            if (haveFilter != true) {
                self.filters[item].value = true;
            }
        }

        function toggleMenu() {
            var debounceFn = debounce(function () {
                $mdSidenav('left-menu')
                    .toggle();
            }, 100)();
        }

        function isOpenMenu() {
            return $mdSidenav('left-menu').isOpen();
        };

        function editarContato() {
            self.oldContato = angular.copy(self.selected);
            self.editando = true;
            self.alterado = false;
            $scope.$broadcast('contatoEdit');
        }

        function processKeyContatoSearch(event) {
            if (event.which == 13) {

            }

            if (event.which == 27) {
                self.filterText = "";
                self.filter();
            }
        }

        function cancelarEdicao() {
            self.editando = false;
            self.alterado = false;
            angular.copy(self.oldContato, self.selected);
            selectContatoById(self.selected._id, true);
            //getAllContatos(self.selected._id);

            /*
            contatoService.getById(self.selected._id).then(function(found) {
                self.selected = found;
                selectContatoById(self.selected._id, true);
            });
            */

        }

        function selectContato(contato, scroll) {
            toggleMenu();
            self.selected = angular.isNumber(contato) ? self.contatos[contato] : contato;
            $timeout(function () {
                if (scroll && self.selected.visible != true) {
                    var old = $location.hash();
                    $location.hash('contatoItem' + self.selected._id.id);
                    $anchorScroll();
                    //reset to old to keep any additional routing logic from kicking in
                    $location.hash(old);

                    /*
                    var container = angular.element(document.getElementById('contato-list'));
                    var cc = angular.element(document.getElementById('contatoItem' + self.selected._id.id));

                    container.scrollToElementAnimated(cc);
                    */
                }
            }, 100);

        }

        function selectContatoById(id, scroll) {
            for (let i = 0; i < self.contatos.length; i++) {
                var element = self.contatos[i];
                if (element._id.id == id) {
                    selectContato(element, scroll);
                    break;
                }
            }
        }

        function deleteContato($event) {

            var selectedIndex = 0;

            for (let i = 0; i < self.contatos.length; i++) {
                if (self.contatos[i]._id.id == self.selected._id.id) {
                    selectedIndex = i;
                    break;
                }
            }

            var confirm = $mdDialog.confirm()
                .title('Remover contato?')
                .content('Quer mesmo remover este contato?')
                .ok('Sim')
                .cancel('Não')
                .targetEvent($event);
            $mdDialog.show(confirm).then(function () {
                contatoService.destroy(self.selected._id).then(function (affectedRows) {
                    self.contatos.splice(selectedIndex, 1);
                    cancelarEdicao();

                    if (selectedIndex - 1 >= 0) {
                        selectContato(selectedIndex - 1)
                    }
                });
            }, function () { });
        }

        function saveContato($event) {
            prepareToSave(self.selected);

            if (self.selected != null && self.selected._id != null) {
                contatoService.update(self.selected).then(function (created) {
                    getAllContatos(self.selected._id);
                    self.editando = false;
                    self.alterado = false;
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Contato editado!')
                            .position('top right')
                            .hideDelay(2000)
                    );
                });
            } else {
                contatoService.create(self.selected).then(function (created) {
                    getAllContatos(created._id);
                    self.editando = false;
                    self.alterado = false;
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Contato salvo!')
                            .position('top right')
                            .hideDelay(2000)
                    );
                });
            }
        }

        function prepareToSave(contato) {
            var telefones = contato.telefones;
            var enderecos = contato.enderecos;

            if (telefones != undefined) {
                telefones.forEach(function (element, index) {
                    if (element.numero == "") {
                        contato.telefones.splice(index, 1);
                    }
                }, this);
            }

            if (enderecos != undefined) {
                enderecos.forEach(function (element, index) {
                    if (element.endereco == "") {
                        contato.enderecos.splice(index, 1);
                    }
                }, this);
            }
        }

        function createContato() {
            self.selected = {
                nome: "",
                sobrenome: "",
                descricao: "",
                telefones: [],
                enderecos: []
            };
            self.editando = true;
            self.alterado = false;
            $scope.$broadcast('contatoEdit');
        }

        function getAllContatos(id) {
            contatoService.getContatos().then(function (contatos) {
                self.contatos = [].concat(contatos);
                if (contatos.length > 0) {
                    if (id != undefined) {
                        selectContatoById(id);
                    } else {
                        self.selected = contatos[0];
                    }
                }

            });
        }

        function filterContato() {
            if (self.filterText == null || self.filterText == "") {
                getAllContatos();
            } else {
                contatoService.getByName(self.filters, self.filterText).then(function (contatos) {
                    self.contatos = [].concat(contatos);

                });
            }
        }

        function adicionarEndereco() {
            if (!self.selected.enderecos)
                self.selected.enderecos = new Array();
            self.selected.enderecos.push({
                tipo: "",
                endereco: ""
            });
        }

        function adicionarTelefone() {
            if (!self.selected.telefones)
                self.selected.telefones = new Array();
            self.selected.telefones.push({
                tipo: "",
                numero: ""
            });
        }

        function clone(obj) {

            var src = JSON.parse(JSON.stringify(obj));

            if (src === null || typeof (src) !== 'object' || 'isActiveClone' in src)
                return src;

            var temp;

            if (src instanceof Date)
                temp = new src.contructor();
            else
                temp = src.constructor();

            for (var key in src) {
                if (Object.prototype.hasOwnProperty.call(src, key)) {
                    src['isActiveClone'] = null;
                    temp[key] = clone(obj[key]);
                    delete src['isActiveClone'];
                }
            }
            return temp;
        }

        function contactInView(index, inview, contato) {
            contato.visible = inview;
        }


        ipcRenderer.on('doControllCall', (event, functionName, args) => {
            switch (functionName) {
                case 'checkUpdate':
                    ipcRenderer.send('check-for-update');
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Procurando atualizações...')
                            .position('top right')
                            .hideDelay(2000)
                    );
                    break;
                case 'noUpdateAvailable':
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Você já está utilizando a última versão!')
                            .position('top right')
                            .hideDelay(2000)
                    );
                    break;
                case 'updateAvailable':
                    $mdToast.show(
                        $mdToast.simple()
                            .content('Uma nova atualização foi encontrada! Aguarde...')
                            .position('top right')
                            .hideDelay(2000)
                    );
                    break;
                case 'displayMessage':
                    $mdToast.show(
                        $mdToast.simple()
                            .content(args)
                            .position('top right')
                            .hideDelay(5000)
                    );
                    break;
            }
        });
    }

})();