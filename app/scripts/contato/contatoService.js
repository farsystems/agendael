(function() {
    'use strict';

    var {
        app
    } = require('electron').remote;
    var tungus = require('tungus');
    var mongoose = require('mongoose');
    var fs = require('fs');
    var Schema = mongoose.Schema;
    var contatoSchema = {
        nome: String,
        sobrenome: String,
        nome_normalizado: String,
        descricao: String,
        email: String,
        enderecos: [{
            tipo: String,
            endereco: String
        }],
        telefones: [{
            tipo: String,
            numero: String
        }]
    };
    var schema = Schema(contatoSchema);
    var Contato = mongoose.model('Contato', schema, 'contatos_v2');
    var dataDirectory = app.getPath('userData') + '/data';
    if (!fs.existsSync(dataDirectory)) {
        fs.mkdirSync(dataDirectory);
    }

    mongoose.connect('tingodb://' + dataDirectory, function(err) {
        // if we failed to connect, abort
        if (err) throw err;
    });

    angular.module('app')
        .service('contatoService', ['$q', ContatoService]);

    function ContatoService($q) {
        return {
            getContatos: getContatos,
            getById: getContatoById,
            getByName: getContatoByName,
            create: createContato,
            destroy: deleteContato,
            update: updateContato
        };

        function getContatos() {
            var deferred = $q.defer();
            Contato.find({}, null, {
                sort:  { nome_normalizado: 1}
            }, function(err, rows) {
                //console.log(rows);
                if (err) deferred.reject(err);
                deferred.resolve(rows);
            });

            return deferred.promise;
        }

        function getContatoById(id) {
            var deferred = $q.defer();

            Contato.findById(id, function(err, rows) {
                if (err) deferred.reject(err);
                deferred.resolve(rows);
            });

            return deferred.promise;
        }

        function getContatoByName(filters, arg) {
            var deferred = $q.defer();

            var filterOr = [];
            for (const f of filters) {
                if (f.value == true) {
                    var v = {};
                    v[f.field] = new RegExp(arg, 'i');
                    filterOr.push(v);
                }
            }
            Contato.find().sort('nome_normalizado').or(filterOr).exec(function(err, rows) {
                if (err) deferred.reject(err);
                deferred.resolve(rows);
            });

            return deferred.promise;
        }

        function createContato(contato) {
            var deferred = $q.defer();
            Contato.create({
                nome: contato.nome,
                sobrenome: contato.sobrenome,
                nome_normalizado: `${contato.nome} ${contato.sobrenome}`.toLowerCase(),
                descricao: typeof contato.descricao == 'undefined' ? '' : contato.descricao,
                email: typeof contato.email == 'undefined' ? '' : contato.email,
                enderecos: contato.enderecos,
                telefones: contato.telefones
            }, function(err, created) {
                if (err) deferred.reject(err);
                deferred.resolve(created);

            });
            return deferred.promise;
        }

        function deleteContato(id) {
            var deferred = $q.defer();
            //console.log(id);
            Contato.remove({
                _id: id
            }, function(err, removed) {
                if (err) deferred.reject(err);
                deferred.resolve(removed);
            });
            return deferred.promise;
        }

        function updateContato(contato) {
            var deferred = $q.defer();
            Contato.update({
                _id: contato._id
            }, {
                $set: {
                    nome: contato.nome,
                    sobrenome: contato.sobrenome == null ? '' : contato.sobrenome || '',
                    nome_normalizado: `${contato.nome} ${contato.sobrenome == null ? '' : contato.sobrenome || ''}`.toLowerCase(),
                    descricao: contato.descricao == null ? '' : contato.descricao || '',
                    email: contato.email == null ? '' : contato.email || '',
                    enderecos: contato.enderecos || '',
                    telefones: contato.telefones || ''
                }
            }, (err) => {
                if (err) {
                    console.log(err);
                    deferred.reject(err);
                }
                deferred.resolve();
            });
            return deferred.promise;
        }
    }
})();