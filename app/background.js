// This is main process of Electron, started as first thing when your
// app starts. This script is running through entire life of your application.
// It doesn't have any windows which you can see on screen, but we can open
// window from here.

function start() {
    if (require('electron-squirrel-startup')) return;
}
start();


import {
    app,
    Menu,
    autoUpdater,
    dialog,
    ipcMain
} from 'electron';
import mainMenuTemplate from './helpers/main_menu_template';
import createWindow from './helpers/window';
var fs = require('fs');

var argv = require('yargs').argv;
var isAutoUpdate = true;
const appVersion = require('./package.json').version;
const os = require('os').platform();
const arch = require('os').arch();

// Special module holding environment variables which you declared
// in config/env_xxx.json file.
import env from './env';

var mainWindow;


// var updateFeed = 'http://localhost:9003/updates/latest/' + os;
var updateFeed = 'http://farsystems.com.br/agenda/download/' + os + '/releases';

if (env.name === 'production') {
    updateFeed = 'http://farsystems.com.br/agenda/download/' + os + '/releases';
}

if (os != 'darwin') {
    updateFeed = `https://dl.bintray.com/frieck/nupkg${arch == 'x64' ? 'x64' : 'x86'}`
}


var setApplicationMenu = function (mw) {
    /*if (env.name !== 'production') {
        menus.push(devMenuTemplate);
    }*/
    Menu.setApplicationMenu(Menu.buildFromTemplate(mainMenuTemplate(mw)));
};

var checkMigration = function () {
    return new Promise(function (resolve, reject) {
        /* Verifica se não existe a base migrada e migra ela*/
        var dataPath = app.getPath('userData') + '/data';
        var dataFile = dataPath + '/contatos_v2';
        if (!fs.existsSync(dataFile)) {
            var Engine = require('tingodb')();
            var db = new Engine.Db(dataPath, {});
            var originalCollection = db.collection("contatos");
            var newColletion = db.collection("contatos_v2");

            originalCollection.find({}).toArray(function(error, docs) {
                for (var i = 0; i < docs.length; i++) {
                    docs[i].nome_normalizado = `${docs[i].nome} ${docs[i].sobrenome}`.toLowerCase(),
                    newColletion.insert(docs[i]);
                }
                db.close();
                resolve(true);
            });
        }
        else {
            resolve(false);
        }
    });
}

app.on('ready', function () {

    checkMigration().then(function (migrated) {
        mainWindow = createWindow('main', {
            width: 1000,
            height: 600
        });

        mainWindow.loadURL('file://' + __dirname + '/app.html');

        if (env.name !== 'production') {
            mainWindow.openDevTools();
        }

        if(migrated) {
            setTimeout(function(){
                mainWindow.webContents.send('doControllCall', 'displayMessage', 'Base de Dados migrada para a nova versão!');
            }, 3000);
        }

        setApplicationMenu(mainWindow);
    });


});

autoUpdater.on('update-available', () => {
    console.log("Update is available!");
    if (!isAutoUpdate)
        mainWindow.webContents.send('doControllCall', 'updateAvailable');
});

autoUpdater.on('update-not-available', () => {
    console.log("Update not available!");
    if (!isAutoUpdate)
        mainWindow.webContents.send('doControllCall', 'noUpdateAvailable');
})

autoUpdater.on('update-downloaded', (event, releaseNotes, releaseName, releaseDate, updateURL) => {
    var index = dialog.showMessageBox(mainWindow, {
        type: 'info',
        buttons: ['Reiniciar', 'Instalar depois'],
        title: "Agenda Pessoal",
        message: 'Uma nova versão foi encontrada!\nPor favor, reinicie a aplicação para instalar.'
    });

    if (index === 1) {
        return;
    }

    autoUpdater.quitAndInstall();
});

app.on('window-all-closed', function () {
    app.quit();
});

ipcMain.on('check-for-update', (event) => {
    isAutoUpdate = false;
    checkForUpdates();
})

setTimeout(function () {
    checkForUpdates()
}, 1000 * 60 * 5);

function checkForUpdates() {
    console.log("Checking for updates...");
    autoUpdater.setFeedURL(updateFeed + '?v=' + appVersion);
    autoUpdater.checkForUpdates();
}