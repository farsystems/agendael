'use strict';
var Q = require('q');


function test() {
    var _test = Q.all([getPromise(1), getPromise(2), getPromise(3)]);
    _test.spread(function (one, two, three) {
        console.log("one.." + one);
        console.log("two.." + two);
        console.log("three.." + three);
    })
}

function getPromise(value) {
    var deferred = Q.defer();
    setTimeout(function (value) {
        let test = Math.random();
        console.log("returning " + test);
        deferred.resolve(test);
    }, 2000);
    return deferred.promise;
}
test();